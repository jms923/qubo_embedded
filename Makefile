TOOLCHAIN_PREFIX ?= arm-none-eabi-
CC := $(TOOLCHAIN_PREFIX)gcc
OBJCOPY := $(TOOLCHAIN_PREFIX)objcopy

SRCDIR := src
OBJDIR := obj
INCDIR := include

INCFLAGS = -I./libopencm3/include -I./$(INCDIR)
LDFLAGS = -static -nostartfiles -T ld.stm32.basic
LDFLAGS += -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group

CFLAGS = -std=c11 -g3 -Os -fno-common -ffunction-sections -fdata-sections

# These flags are specific for our board
CFLAGS += -mcpu=cortex-m0plus -mthumb -msoft-float

# Modify these depending on what you're compiling, these are for
# compile-time definitions
CFLAGS += -DRCC_LED1=RCC_GPIOA -DPORT_LED1=GPIOA -DPIN_LED1=GPIO5
CFLAGS += -DSTM32G0 -DLITTLE_BIT=400000

LIBS = -L./libopencm3/lib -lopencm3_stm32g0

# Modify this SRCS line to add source files to compile
SRCS := main.c
OBJS := $(SRCS:.c=.o)

SRCS := $(addprefix $(SRCDIR)/, $(SRCS))
OBJS := $(addprefix $(OBJDIR)/, $(OBJS))

all: obj opencm3 image.bin

obj:
	mkdir -p obj

image.bin: image.elf
	$(OBJCOPY) -Obinary image.elf image.bin

image.elf: $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS) $(LIBS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(INCFLAGS) $(CFLAGS) -c $< -o $@

opencm3:
	$(MAKE) -C libopencm3

clean:
	- rm image.elf image.bin obj/*

.PHONY: all opencm3 clean
