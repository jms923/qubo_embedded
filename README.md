# Qubo Embedded

## Installing dependencies

### Installing easy dependencies

Debian/Ubuntu:

```
apt-get install make cmake
```

### Installing ARM GNU toolchain

Download ARM GNU toolchain v11.3:

- [x86_64 host](https://developer.arm.com/-/media/Files/downloads/gnu/11.3.rel1/binrel/arm-gnu-toolchain-11.3.rel1-x86_64-arm-none-eabi.tar.xz) -- If you have an x86_64 computer
- [aarch64 host](https://developer.arm.com/-/media/Files/downloads/gnu/11.3.rel1/binrel/arm-gnu-toolchain-11.3.rel1-aarch64-arm-none-eabi.tar.xz?rev=82c9a3730e454ab6b8101952cd700cda&hash=A484F380E7D73DF3C5F13CA6EBB954D5) -- If you have an ARM computer (e.g. Apple Silicon M-series)

Install ARM GNU toolchain:

The file name may change in the future and will depend on which one you downloaded, so we will write in all caps to indicate a place where you need to modify the command. The downloaded file is in `.tar.xz` format (like a zip file). Change directory (`cd`) to the folder where you downloaded the file to. The following commands describe the general idea of what you need to do, but you will need to fill in the name of the file you downloaded.

```
$ tar -xvf NAME_OF_FILE.tar.xz
$ mkdir -p ~/opt
$ mv NAME_OF_EXTRACTED_FOLDER ~/opt/.
```

Now you need to add `~/opt/NAME_OF_EXTRACTED_FOLDER/bin` to your `PATH`.

```
echo 'export PATH="$PATH:$HOME/opt/NAME_OF_EXTRACTED_FOLDER/bin"' >> ~/.bashrc
```

## Building

It is expected that you will use Linux for building and developing. Note that
this is not resource-intensive however, so you are welcome to use a VM.

To compile, just run `make`.

See the Makefile for information on how to add/create new source files.

### Flashing with `st-flash`

For flashing the compiled binary to the hardware, we recommend one of two
options:
- (**Preferred**) Use "drag-n-drop" to flash the hardware. When you plug in
  the USB, a device should pop in your file manager as a mounted device. You
  can then flash by copying `image.bin` via the GUI or the CLI to the root
  of that device (that is, without entering any directories on that device).
- Using [stlink](https://github.com/stlink-org/stlink) or some similar
  tool. The version that lives in the Debian and Ubuntu packages is outdated
  though, you will have to compile stlink from scratch. If you pursue this
  method, the command to flash (using stlink) is
```
$ st-flash write image.bin 0x8000000
```

## Useful References
### General
- libopencm3 documentation can be found [here](http://libopencm3.org/docs/latest/stm32g0/html/modules.html).
- For a bunch of reference documentation on things like FreeRTOS and our
microcontroller, check out the [Google Drive](https://drive.google.com/drive/u/1/folders/1E0iK5DRwgsf7khUXgLCXMgLPK_xRI3Ca) (if you cannot access this let us know
in the Slack channel).

### Thrusters
- Data/Information for our thrusters can be found [here](https://bluerobotics.com/store/thrusters/t100-t200-thrusters/t200-thruster-r2-rp/).
- Data/Information for our ESCs can be found [here](https://bluerobotics.com/store/thrusters/speed-controllers/besc30-r3/).

### Power Board
- TBD

## Design Choices (for those Curious)

Right now this repository houses all the code that is targeted for our embedded
system, in particular code that resides on our STM32G0B1RE MCUs.

For the hardware abstraction layer, we have chosen to use libopencm3.
There are numerous reasons for this decision, the primary reason being that
the official HAL libraries were designed to be compiled with their
STM32CubeIDE. For users who just wanted to use a raw ARM-GCC toolchain and
make, it was noticeably more difficult to convert the provided build system.

We have opted to use FreeRTOS as our real-time operating system, out of
necessity for running multiple tasks per MCU, and the ubiquity of FreeRTOS.